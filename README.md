# OrganizeItNow Summary

A project and task manager application is a tool that allows users to manage and organize their projects and tasks. The application allows users to create projects, add tasks to each project, and track the progress of each task. The application also includes a login and sign-up system, allowing users to access their account and manage their projects and tasks. The application is composed of seven HTML templates, seven views, and two models - the Project model and the Task model - which are used to manage and store information about projects and tasks.

# How to get started

- Download the project to your local machine
- Create and activate a virtual environment
- Make sure you do this in a new terminal window or
  - python -m venv .venv
  - source ./.venv/bin/activate # macOS
  - ./.venv/Scripts/Activate.ps1 # Windows

# Photos coming in soon
